<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id'=>1,
                'name'=>'Admin',
                'email'=>'Indra@goodstuff.com',
                'password'=>Hash::make('admin'),
                'role'=>'Admin'
            ],
            [
                'id'=>2,
                'name'=>'Muhamat alhakim',
                'email'=>'Muhamat@gmail.com',
                'password'=>Hash::make('Muhamat'),
                'role'=>'Customer'
            ],
            [
                'id'=>3,
                'name'=>'Oki',
                'email'=>'okioke@gmail.com',
                'password'=>Hash::make('Oki'),
                'role'=>'Customer'
            ],
            [
                'id'=>4,
                'name'=>'Emillie Norton',
                'email'=>'emillie_norton@gmail.com',
                'password'=>Hash::make('johndoe'),
                'role'=>'Customer'
            ],
        ]);
    }
}